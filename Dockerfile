FROM python:3.9

COPY . .

RUN pip install --upgrade pip && pip3 install -r requirements.txt

CMD ["python3", "-m", "bot"]
